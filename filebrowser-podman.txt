Image
docker.io/filebrowser/filebrowser:latest

Ports
0.0.0.0:8080 → 80/tcp

Volumes
/var/DATA/AppData/FileBrowser/config ↔ /db
/var/DATA/storage ↔ /srv

Environment variables
TZ=Australia/Melbourne
PUID=1000
PGID=1000
