Image
docker.io/jellyfin/jellyfin:latest

Command

Ports
0.0.0.0:8096 → 8096/tcp

Volumes
/var/DATA/AppData/Jellyfin/config ↔ /config
/var/DATA/AppData/Jellyfin/cache ↔ /cache
/var/DATA/Media ↔ /media
